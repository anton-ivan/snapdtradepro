Rails.application.routes.draw do
  resources :checklists do
    resources :checklist_items do
      member do
        post :completed
        post :uncompleted
      end
    end
  end
  resources :valuation_url_comments

  get 'members/index'

  ActiveAdmin.routes(self)

  devise_for :users, controllers: { sessions: 'sessions' }

  root to: "pages#home"

  resources :pre_launch_registrations, only: [:new, :create] do
    get :success, on: :collection
  end

  resources :organizations, only: [:show, :edit, :update] do
    resources :customers, except: :destroy do
      get :archived, on: :collection
      put :archive, on: :member
      put :bulk_archive, on: :collection
    end

    resources :appraisals, except: :destroy do
      get :archived, on: :collection
      put :archive, on: :member
      put :clone, on: :member
      get :sales_sheet, on: :member
      get :appraisal_sheet, on: :member
      put :bulk_archive, on: :collection
      get :remove_entry, on: :member
      get :history, on: :member
      post :comment, on: :member
      get :valuations, on: :member
      post :pin, on: :member
      post :unpin, on: :member
      get :reveal_comments, on: :member
      get :hide_comments, on: :member
      put :valuation, on: :member

      resources :discussions do
        resources :discussion_comments
      end

      resources :valuation_urls
    end

    resources :members, only: [:index, :show, :update] do
      post :invite_user, on: :collection
      get :reset_password, on: :member
      get :resend_invitation, on: :member
    end
  end

  resources :members, only: [] do
    get :accept_invitation, on: :collection
  end

  resources :attachments

  resources :reports do
    get :inventory_commitments, on: :collection
  end

  resources :print_templates

  resources :appraisal_categories, only: [:show]

  namespace :api do
    api version: 1, module: 'v1' do
      resources :sessions, only: [:create]

      resources :appraisals, only: [:index] do
        resources :appraisal_categories, only: [:show]
      end

      resources :organizations, only: [:index] do
        resources :appraisals, only: [:create, :show, :update]
        resources :customers, only: [:create, :show, :update]
      end
    end
  end
end
