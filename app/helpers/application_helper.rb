module ApplicationHelper
  def flash_messages(opts={})
    @layout_flash = opts.fetch(:layout_flash) { true }

    capture do
      flash.each do |name, msg|
        concat content_tag(:div, msg.html_safe, id: "flash_#{name}")
      end
    end
  end

  def show_left_nav?
    return false if @show_left_nav == false

    user_signed_in? && @organization
  end

  def show_layout_flash?
    @layout_flash.nil? ? true : @layout_flash
  end

  def breadcrumbs
    @breadcrumbs ||= []
    @breadcrumbs
  end

  def pdf_image_tag(src, options = {})
    return if src.nil?
    src = gsub_https(src)
    image_tag src, options
  end

  def gsub_https(src)
    src.gsub!('https://', 'http://')
    src
  end

  def pdf_preview_link(organization, appraisal)
    eval("#{params[:action]}_organization_appraisal_path(organization, appraisal, format: :pdf)")
  end

  def filepicker_url(image, size: :small)
    return image unless image.include?("filepicker")

    options = { cache: true }

    if size == :small
      image = "#{image}/convert"
      options.merge!({ w: "200", h: "200" })
    end

    "#{image}?#{options.to_query}"
  end

  def display_column_header(default_text,column_data)
    return default_text if column_data.nil?
    if column_data.template_field.present?
      column_data.template_field.name.humanize
    else
      column_data.static_field.humanize
    end
  end

  def display_column_data(column_data=nil,object=nil, default_attr=nil)
    return yield if block_given?
    return object.send(default_attr.to_sym) if column_data.nil?

    if column_data.template_field.present?
      object.field_value(column_data.template_field_id)
    else
      object.send(column_data.static_field.to_sym)
    end
  end

end
