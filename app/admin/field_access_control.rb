ActiveAdmin.register FieldAccessControl do

  actions :all
  index do
    column :organization_id, as: :select, collection: Organization.all.collect { |organization| [organization.name,organization.id]}
    column "Organization Name" do |post|
      link_to post.organization.name, admin_organization_path(post.organization)
    end
    column :template_field_id, as: :select, collection: TemplateField.all.collect { |field| [field.name, field.id]}
    column "Template Field Name" do |post|
      link_to post.template_field.name, admin_template_field_path(post.template_field)
    end
    column :view_permission
    column :edit_permission
    actions
  end

  form do |f|
    f.inputs 'Field Access Control Details' do
      f.input :view_permission, as: :boolean
      f.input :edit_permission, as: :boolean

      f.inputs do
        f.template.render partial: 'organizations_fac', locals: { f: f }
      end

      f.inputs do
        f.template.render partial: 'template_fields_fac', locals: { f: f }
      end
    end
    f.actions
  end
end
