ActiveAdmin.register AppraisalColumn do
  menu priority: 7

  actions :all
  form do |f|
    f.inputs do
      f.input :column_name, as: :select, collection: AppraisalColumn::COLUMNS, include_blank: false
      f.input :organization, as: :select, collection: Organization.order(:name), include_blank: false

      f.inputs do
        f.template.render partial: 'template_fields', locals: { f: f }
      end
    end
    f.actions
  end
  index do
    id_column
    column :column_name
    column :template_field, :sortable => 'template_fields.name'
    column :static_field
    column "Label", :sortable => 'template_fields.label' do |appraisal_column|
      appraisal_column.template_field.try(:label)
    end
    column :organization
    actions
  end

  controller do
    def scoped_collection
      end_of_association_chain.includes(:template_field)
    end
  end
end
