class FieldAccessControl < ActiveRecord::Base

  belongs_to :organization
  belongs_to :template_field

  scope :by_organization, -> org { where(organization_id: org) }
end
