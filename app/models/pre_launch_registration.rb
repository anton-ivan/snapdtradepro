class PreLaunchRegistration
  include ActiveModel::Model

  attr_accessor :first_name, :last_name, :email

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true

  def persisted?
    false
  end
end
