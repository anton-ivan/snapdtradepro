class TemplateField < ActiveRecord::Base
  FIELD_TYPES = ["Text Field", "Text Area", "Photo", "Select",
                 "Check Boxes", "Numeric", "Date Field", "DateTime Field"]
  TYPES_WITH_CHOICES = ["Select", "Check Boxes"]
  TYPES_WITH_FORMATTING = ["Numeric", "Text Field"]

  has_and_belongs_to_many :appraisal_categories
  has_many :field_access_control

  validates :name, presence: true, uniqueness: true
  validates :label, presence: true
  validates :field_type, presence: true
  validates :weight, presence: true, numericality: true, inclusion: 0..100
  validates :choices, presence: { if: :choices_required? }
  validate :formatting_parses

  scope :photos, -> { where(field_type: "Photo") }
  scope :non_photos, -> { where.not(field_type: "Photo") }
  scope :non_text_areas, -> { where.not(field_type: "Text Area") }
  scope :numeric, -> { where(field_type: "Numeric") }
  scope :weighted, -> { order("weight ASC") }
  scope :by_name, -> name { where(name: name) }
  scope :by_label, -> label { where(label: label) }
  scope :editable, -> { where(editable_by_user: true) }
  scope :has_access_control, ->  { where(has_access_control: true) }

  def type_lookup
    field_type.downcase.gsub(" ", "_")
  end

  def to_sym
    label.downcase.to_sym
  end

  def web_label
    print_label.nil? ? label : print_label
  end

  def choices_required?
    TYPES_WITH_CHOICES.include?(field_type)
  end

  def has_formatting?
    TYPES_WITH_FORMATTING.include?(field_type)
  end

  def choices_array
    choices.split("\r\n")
  end

  def parsed_formatting
    formatting.present? ? YAML.load(formatting) : {}
  end

  def numeric?
    field_type == "Numeric"
  end

  private

  def formatting_parses
    return unless numeric?
    YAML.load(formatting)
  rescue Psych::SyntaxError
    errors.add(:formatting, "is not formatted properly")
  end
end
