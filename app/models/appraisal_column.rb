class AppraisalColumn < ActiveRecord::Base
  COLUMNS = %w(column1 column2 column3 column4 column5 column6)
  validates :column_name, uniqueness: {scope: :template_field_id}
  belongs_to :template_field
  belongs_to :organization

  before_save :ensure_single_field_update

  def ensure_single_field_update
    if self.template_field_id_changed?
      self.static_field = nil
    elsif self.static_field_changed?
      self.template_field_id = nil
    end
  end
end
