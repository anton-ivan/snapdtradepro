class AppraisalTemplate < ActiveRecord::Base
  has_and_belongs_to_many :appraisal_categories
  has_and_belongs_to_many :organizations
  has_many :print_templates

  validates :label, presence: true
  validates :name, presence: true, uniqueness: true

  def visible_categories
    appraisal_categories.joins(:template_fields).uniq
  end

  def self.clone(template)
    cloned = template.dup
    cloned.name = "Cloned - #{cloned.name} #{AppraisalTemplate.count}"
    cloned.save!

    # clone appraisal categories
    template.appraisal_categories.each do |category|
      cloned.appraisal_categories << category
    end

    cloned
  end
end
