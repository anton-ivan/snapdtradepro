class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :name, :acronym, :appraisal_templates

  has_many :customers

  def appraisal_templates
    object.appraisal_templates.map do |template|
      # NAME SHOULD BE REMOVED FROM THIS WHEN IT'S SAFE
      template.attributes.symbolize_keys.slice(:id, :name, :label)
    end
  end
end
