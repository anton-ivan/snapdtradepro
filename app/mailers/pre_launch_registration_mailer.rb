class PreLaunchRegistrationMailer < ActionMailer::Base
  def registration_email(pre_launch_registration)
    @pre_launch_registration = pre_launch_registration

    mail to: ENV["PRE_LAUNCH_REGISTRATION_EMAIL"],
         subject: "Snap Trade Pro - Pre Launch Registration",
         from: @pre_launch_registration.email
  end
end
