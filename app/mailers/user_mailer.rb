class UserMailer < ActionMailer::Base
  default from: "\"Snap Trade Pro\" <noreply@snaptradepro.com>"

  def notify_reactivation(user)
    @user = user
    mail to: user.email, subject: "Snap Trade Pro - Reactivation"
  end
end
