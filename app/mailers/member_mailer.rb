class MemberMailer < ActionMailer::Base
  default from: "\"Snap Trade Pro\" <noreply@snaptradepro.com>"
  
  def send_invitation(membership, member)
    @member = member
    @membership = membership
    mail to: @member.email, subject: "Snap Trade Pro - Invitation"
  end

  def notify_invitation_pending_back_to_invitor(membership)
    @membership = membership
    @invitor = membership.invitor
    @member = membership.user

    mail to: @invitor.email, subject: "STP Member Invite 24 Hours Old"
  end

  def notify_invitation_revoked_to_invitor(membership)
    @membership = membership
    @invitor = membership.invitor
    @member = membership.user

    mail to: @invitor.email, subject: "STP Member Invite 30 Days Old"
  end

  def notify_invitation_deactivated_to_invitee(membership)
    @membership = membership
    @invitor = membership.invitor
    @member = membership.user

    mail to: @member.email, subject: "Your STP Member Invitation has lapsed"
  end

  def notify_news_of_being_org_admin(org)
    @admin = org.admin
    @organization = org
    mail to: @admin.email, subject: "Your are an organization admin for #{org.name}"
  end

  def notify_news_of_being_org_owner(org)
    @owner = org.owner
    @organization = org
    mail to: @owner.email, subject: "Your are an organization owner for #{org.name}"
  end
end
