# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Rails.env.development?
  user = CreateAdminService.new.call
  puts 'CREATED ADMIN USER: ' << user.email

  puts 'Create organization'
  org = FactoryGirl.create(:organization)

  puts 'Create 20 appraisals'

  2.times do
    FactoryGirl.create(:appraisal, user_id: 1, organization: org)
  end
end

Role.create(name: "Org Owner", profile: "Owner")
Role.create(name: "Org Admin", profile: "Admin")
Role.create(name: "Rep",       profile: "Member")
Role.create(name: "Rep",       profile: "Sales Mgr")
Role.create(name: "Rep",       profile: "Sales Coordinator")
Role.create(name: "Rep",       profile: "Sales")

Role.create(name: "Agent",     profile: "Freelancer")
Role.create(name: "Agent",     profile: "Asset Controller")
Role.create(name: "Agent",     profile: "Potential Buyers")
Role.create(name: "Agent",     profile: "Recovery")
Role.create(name: "Agent",     profile: "Transport")
Role.create(name: "Agent",     profile: "Marketing")
