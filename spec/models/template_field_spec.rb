require 'spec_helper'

describe TemplateField do
  describe "associations" do
    it { should have_and_belong_to_many :appraisal_categories }
  end

  describe "validations" do
    before do
      create(:id_field)
    end

    it { should validate_presence_of :name }
    it { should validate_uniqueness_of :name }
    it { should validate_presence_of :label }
    it { should validate_presence_of :field_type }
  end
end
