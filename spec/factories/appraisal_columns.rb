# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :appraisal_column do
    column_name "MyString"
    template_field_id 1
  end
end
